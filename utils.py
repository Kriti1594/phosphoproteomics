import ast
import os
import pickle
from collections import Counter
from re import findall

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import uniprot
from scipy.stats import f_oneway
from sklearn.decomposition import PCA
from statsmodels.sandbox.stats.multicomp import MultiComparison


def create_dir(file_path):
    formated_file_path = file_path.replace('\\', '/')

    # Checks if the chosen file path is correct.
    if os.path.isdir(formated_file_path) is False:
        print("Incorrect file path chosen.")
        quit()
    else:
        # If the file path exists then the work directory is changed to this file path.
        os.chdir(file_path)

    if os.path.isdir(file_path + '/Phosphoproteomics_Output') is False:
        os.makedirs(file_path + '/Phosphoproteomics_Output')


def find_seqs_ranges(accessions_list, sequences, fastas):
    accession_ranges = []
    accession_seqs = []
    for i, accessions in enumerate(accessions_list):
        obv_ranges = []
        complete_seq = []
        for acc in accessions:
            try:
                start = fastas['sp|' + acc]['sequence'].find(sequences[i]) + 1
                end = start + len(sequences[i]) - 1
                obv_ranges.append([start, end])
                complete_seq.append(fastas['sp|' + acc]['sequence'])
            except KeyError:
                obv_ranges.append(None)
                complete_seq.append(None)
        accession_ranges.append(obv_ranges)
        accession_seqs.append(complete_seq)
    return accession_seqs, accession_ranges


# def find_ranges(accessions_list, sequences, fastas):
#     accession_ranges = []
#     for i, accessions in enumerate(accessions_list):
#         obv_ranges = []
#         for acc in accessions:
#             try:
#                 start = fastas['sp|' + acc]['sequence'].find(sequences[i])
#                 end = start + len(sequences[i])
#                 obv_ranges.append([start, end])
#             except KeyError:
#                 obv_ranges.append(None)
#         accession_ranges.append(obv_ranges)
#     return accession_ranges

def merger(a, b):
    if a is not None:
        sublist = []
        for i in range(len(a)):
            if a[i] is not None:
                sublist.append(a[i])
            else:
                sublist.append(b[i])
        return sublist
    else:
        return None


def merge_lists(lists):
    first_list = lists[0]
    output = [[None] * len(sublist) if sublist is not None else None for sublist in first_list]
    for i_list in lists:
        for i, i_sublist in enumerate(i_list):
            output[i] = merger(output[i], i_sublist)
    return output


def condenser(a, len_unique_databases):
    new_a = []
    for i in range(len(a))[::len_unique_databases]:
        if all(j is None for j in a[i:i + len_unique_databases]):
            new_a.append([None])
        else:
            new_a.append([j for j in a[i:i + len_unique_databases] if j is not None])
    new_a = [i[0] for i in new_a]
    return new_a


def gene_condensed_merger(a, b, len_unique_databases):
    a = condenser(a, len_unique_databases)
    b = condenser(b, len_unique_databases)
    return [a[i] if a[i] is not None and all(j in a[i] is not None for j in a[i]) else b[i] for i in range(len(a))]


def condensed_merger(a, b, len_unique_databases):
    a = condenser(a, len_unique_databases)
    b = condenser(b, len_unique_databases)
    return [a[i] if a[i] is not None and all(j in a[i] is None for j in a[i]) else b[i] for i in range(len(a))]


def gene_condensed_merge_lists(lists, len_unqiue):
    merged_list = [gene_condensed_merger(a, b, len_unqiue) for a, b in list(zip(lists[0], lists[1]))]
    if len(lists) > 2:
        for i_list in lists[2:]:
            merged_list = [gene_condensed_merger(a, b, len_unqiue) for a, b in list(zip(merged_list, i_list))]
    return merged_list


def condensed_merge_lists(lists, len_unqiue):
    merged_list = [condensed_merger(a, b, len_unqiue) for a, b in list(zip(lists[0], lists[1]))]
    if len(lists) > 2:
        for i_list in lists[2:]:
            merged_list = [condensed_merger(a, b, len_unqiue) for a, b in list(zip(merged_list, i_list))]
    return merged_list


def findall_seqs_ranges(accessions_list, sequences, *fastas):
    all_ranges = []
    all_seqs = []
    for fasta in fastas:
        seqids, fasta_output = uniprot.read_fasta(fasta)
        seqs, ranges = find_seqs_ranges(accessions_list, sequences, fasta_output)
        all_ranges.append(ranges)
        all_seqs.append(seqs)
    ranges_output = merge_lists(all_ranges)
    seqs_output = merge_lists(all_seqs)
    return seqs_output, ranges_output


#
# def findall_ranges(accessions_list, sequences, *fastas):
#     all_ranges = []
#     for fasta in fastas:
#         seqids, fasta_output = uniprot.read_fasta(fasta)
#         ranges = find_ranges(accessions_list, sequences, fasta_output)
#         all_ranges.append(ranges)
#     ranges_output = merge_lists(all_ranges)
#     return ranges_output


def findall_ranges(accessions_list, sequences, fastas):
    all_ranges = []
    for fasta in fastas:
        seqids, fasta_output = uniprot.read_fasta(fasta)
        unique_databases = set([i.split('|')[0] for i in seqids])
        ranges = find_ranges(accessions_list, sequences, unique_databases, fasta_output)
        all_ranges.append(ranges)
    ranges_output = range_condensed_merge_lists(all_ranges, len(unique_databases))
    return ranges_output


def find_ranges(accessions_list, sequences, unique_databases, fastas):
    accession_ranges = []
    for i, accessions in enumerate(accessions_list):
        obv_ranges = []
        for database in unique_databases:
            for acc in accessions:
                if fasta_checker(fastas, database, acc):
                    start = fastas['{}|{}'.format(database, acc)]['sequence'].find(sequences[i])
                    end = start + len(sequences[i])
                    obv_ranges.append([start + 1, end])
                else:
                    obv_ranges.append(None)
        accession_ranges.append(obv_ranges)
    return accession_ranges


def range_condensed_merger(a, b, len_unique_databases):
    a = range_condenser(a, len_unique_databases)
    b = range_condenser(b, len_unique_databases)
    return [a[i] if a[i] is not None else b[i] for i in range(len(a))]


def range_condensed_merge_lists(lists, len_unqiue):
    merged_list = [range_condensed_merger(a, b, len_unqiue) for a, b in list(zip(lists[0], lists[1]))]
    if len(lists) > 2:
        for i_list in lists[2:]:
            merged_list = [range_condensed_merger(a, b, len_unqiue) for a, b in list(zip(merged_list, i_list))]
    return merged_list


def range_condenser(a, len_unique_databases):
    sud_a_start = a[:int(len(a) / len_unique_databases)]
    sud_a_end = a[int(len(a) / len_unique_databases):]
    new_a = [sud_a_start[i] if sud_a_start[i] is not None else sud_a_end[i] for i in range(int(len(a) / 2))]
    return new_a


def findall_unique_mod_types(modifications):
    """
    Determine the key features that appear in the modifications column.
    :param modifications:
    :return:
    """

    modifications_expanded_list = []
    for i in modifications:
        modifications_expanded_list.append(i.split(' '))

    modifications_expanded_list = [item for sublist in modifications_expanded_list for item in sublist]

    mod_types = [i for i in modifications_expanded_list if (i.find('[') == -1 and i.find('x') != -1)]
    mod_types = [i[2:] for i in mod_types]
    return list(Counter(mod_types).keys())


def mods_dict_updater(mods_dict, unique_mod_types, motif_mods):
    """
    Create a dictionary that breaks the modifcations column and creates individual lists for each key
    feature of the modifications.

    :param unique_mod_types:
    :param mods_dict:
    :param motif_mods:
    :return:
    """

    for unique_mod in unique_mod_types:
        mod_value = []
        mod_variables = []
        for i in range(len(motif_mods)):
            if unique_mod in ' '.join(motif_mods[i]):
                for mod in motif_mods[i]:
                    if unique_mod in mod:
                        mod_value.append(mod[:mod.find('x')])
                        mod_variables.append('[' + mod.split(' [')[1])
            else:
                mod_value.append(None)
                mod_variables.append(None)
        mods_dict[unique_mod + '_value'] = mod_value
        mods_dict[unique_mod + '_variables'] = mod_variables

    return mods_dict


def findall_uniprot_ranges(motif_dict, range_column='Protein Accession Ranges'):
    lhs_ranges = []
    for range_list in motif_dict[range_column]:
        if range_list is not None:
            lhs_ranges.append([i[0] - 1 if i is not None else None for i in range_list])
        else:
            lhs_ranges.append(None)
    return lhs_ranges


def fasta_checker(fasta_output, database, accession):
    try:
        var = fasta_output["{}|{}".format(database, accession)]
        return True
    except KeyError:
        return False


def uniprot_description(accessions, seqids, fastas):
    all_protein_names = []
    all_entry_names = []
    all_gene_names = []

    unique_databases = set([i.split('|')[0] for i in seqids])

    for accession_list in accessions:
        protein_names = []
        entry_names = []
        gene_names = []

        for i in range(len(accession_list)):
            for database in unique_databases:
                if fasta_checker(fastas, database, accession_list[i]):
                    fasta_key = "{}|{}".format(database, accession_list[i])
                    description = fastas[fasta_key]['description']
                    start_point = description.index(fasta_key) + len(fasta_key) + 1
                    entry_name = description[start_point:][:description[start_point:].find(' ')]
                    entry_names.append(entry_name)
                    second_start_point = description.index(entry_name) + len(entry_name) + 1
                    protein_name = description[second_start_point:][:description[second_start_point:].find(' OS=')]
                    protein_names.append(protein_name)
                    if 'GN=' in description:
                        gn_start_point = description.find('GN=') + len('GN=')
                        gn_end_point = description[description.find('GN=') + len('GN='):].find(' ')
                        if gn_end_point == -1:
                            gene_name = description[gn_start_point:]
                        else:
                            gene_name = description[gn_start_point:gn_start_point + gn_end_point]
                        gene_names.append(gene_name)
                    else:
                        gene_names.append(None)
                else:
                    protein_names.append(None)
                    entry_names.append(None)
                    gene_names.append(None)

        # gene_names = condensed_merge_lists(gene_names, len(unique_databases))

        # if len(protein_names) != len(accession_list):
        #     difference = len(accession_list) - len(protein_names)
        #     protein_names = protein_names + ([None] * difference)
        # if len(entry_names) != len(accession_list):
        #     difference = len(accession_list) - len(entry_names)
        #     entry_names = entry_names + ([None] * difference)
        # if len(gene_names) != len(accession_list):
        #     difference = len(accession_list) - len(gene_names)
        #     gene_names = gene_names + ([None] * difference)

        all_protein_names.append(protein_names)
        all_entry_names.append(entry_names)
        all_gene_names.append(gene_names)

    return all_protein_names, all_entry_names, all_gene_names, unique_databases


def findall_uniprot_protein_names(accessions, fastas):
    all_protein_names = []
    all_entry_names = []
    all_gene_names = []
    for fasta in fastas:
        seqids, fasta_output = uniprot.read_fasta(fasta)
        protein_names, entry_names, gene_names, unique_databases = uniprot_description(accessions, seqids, fasta_output)
        all_protein_names.append(protein_names)
        all_entry_names.append(entry_names)
        all_gene_names.append(gene_names)
    all_protein_names = gene_condensed_merge_lists(all_protein_names, len(unique_databases))
    all_entry_names = gene_condensed_merge_lists(all_entry_names, len(unique_databases))
    all_gene_names = gene_condensed_merge_lists(all_gene_names, len(unique_databases))
    # all_protein_names = [i if i != [None] else None for i in merge_lists(all_protein_names)]
    # all_entry_names = [i if i != [None] else None for i in merge_lists(all_entry_names)]
    # all_gene_names = [i if i != [None] else None for i in merge_lists(all_gene_names)]
    return all_protein_names, all_entry_names, all_gene_names


def sig_mods_dict_updater(mods_dict, sig_mods_dict, unique_mod_types, p_value=75):
    """
    Create a dictionary that breaks the modifications column and creates individual lists for
    each key feature of the modifications that is statistically significant.
    :param mods_dict:
    :param sig_mods_dict:
    :param unique_mod_types:
    :return:
    """

    for unique_mod in [mod + '_variables' for mod in unique_mod_types]:
        mod_variables_expanded = [i.split('; ') if i is not None else None for i in mods_dict[unique_mod]]
        mod_significant_variables_list = []
        for i in range(len(mod_variables_expanded)):
            mod_significant_variables = []
            if mod_variables_expanded[i] is not None:
                for j in range(len(mod_variables_expanded[i])):
                    mod_variables_expanded[i][j] = mod_variables_expanded[i][j].replace('[', '')
                    mod_variables_expanded[i][j] = mod_variables_expanded[i][j].replace(']', '')
                    if mod_variables_expanded[i][j].find('(') != -1:
                        prob = mod_variables_expanded[i][j][
                               mod_variables_expanded[i][j].find('(') + 1:mod_variables_expanded[i][j].find(')')]
                        if float(prob) >= p_value:
                            mod_significant_variables.append(mod_variables_expanded[i][j])
            if len(mod_significant_variables) > 0:
                mod_significant_variables_list.append(mod_significant_variables)
            else:
                mod_significant_variables_list.append(None)
        sig_mods_dict['significant_' + unique_mod] = mod_significant_variables_list
    return sig_mods_dict


def sig_mods_dict_modified_updater(sig_mods_dict, sig_mods_dict_modified, lhs_range_list):
    """
    Create a dictionary that contains individual lists for each key feature
    of the modifications that is statistically significant.
    The lists within this dictionary will then eventually be brought together to create the Doug Annotations coloumn.

    :param lhs_range_list:
    :param sig_mods_dict:
    :param sig_mods_dict_modified:
    :return:
    """

    for unique_mod in [key for key in sig_mods_dict.keys() if any(value is not None for value in sig_mods_dict[key])]:
        all_significant_variables_modified = []
        all_significant_variables_position = []
        all_significant_variables_letters = []
        significant_variables = sig_mods_dict[unique_mod]
        for i, lhs_range in enumerate(lhs_range_list):
            significant_variables_modified = []
            significant_variables_position = []
            significant_variables_letters = []
            for range_value in lhs_range:
                modified = []
                signif_pos = []
                signif_letters = []
                if range_value is not None:
                    if significant_variables[i] is not None:
                        for j in range(len(significant_variables[i])):
                            new_position = int(range_value) + int(findall(r'\d+', significant_variables[i][j])[0])
                            signif_pos.append(new_position)
                            signif_letters.append(significant_variables[i][j][:1])
                            modified.append(
                                significant_variables[i][j][:1] + str(new_position) + significant_variables[i][j][
                                                                                      significant_variables[i][j].find(
                                                                                          '('):])
                if len(modified) > 0:
                    significant_variables_modified.append(modified)
                else:
                    significant_variables_modified.append(None)
                if len(signif_pos) > 0:
                    significant_variables_position.append(signif_pos)
                else:
                    significant_variables_position.append(None)
                if len(signif_letters) > 0:
                    significant_variables_letters.append(signif_letters)
                else:
                    significant_variables_letters.append(None)

            all_significant_variables_modified.append(significant_variables_modified)
            all_significant_variables_position.append(significant_variables_position)
            all_significant_variables_letters.append(significant_variables_letters)

        sig_mods_dict_modified[unique_mod] = all_significant_variables_modified
        sig_mods_dict_modified[unique_mod + '_positions'] = all_significant_variables_position
        sig_mods_dict_modified[unique_mod + '_letters'] = all_significant_variables_letters

    return sig_mods_dict_modified


def determine_sig_positions(dict, position_names):
    significant_positions = []
    for sublist in list(
            map(list,
                zip(*[dict[label] for label in position_names]))):
        new_sublist = [i for i in sublist if i is not None]
        if len(new_sublist) == 0:
            new_sublist.append(None)
        significant_positions.append(new_sublist)

    total_significant_positions = []
    for sublist in significant_positions:
        total_significant_positions.append([i[0] if i is not None else None for list_i in sublist for i in list_i])
    return total_significant_positions


def doug_annotations(mod_name, accessions, mod_values, sig_mods):
    doug_values = []
    for i, mod_list in enumerate(sig_mods):
        if all(mod is None for mod in mod_list):
            doug_values.append([None])
        else:
            doug_sub_values = []
            for j, mod_sublist in enumerate(mod_list):
                if not mod_sublist is None:
                    doug_obv = []
                    for mod in mod_sublist:
                        doug_obv.append("{} {}x{} {}".format(accessions[i][j], mod_values[i], mod_name, mod))
                    doug_sub_values.append(doug_obv)
                doug_values.append(doug_sub_values)
    return doug_values


def add_doug_annotations(motif_dict, mods_dict, sig_mods_dict_modified):
    mod_names = [mod for mod in sig_mods_dict_modified.keys() if mod.find('_variables_positions') is not -1]
    mod_names = [mod.replace('_variables_positions', '') for mod in mod_names]
    mod_names = [mod.replace('significant_', '') for mod in mod_names]
    accessions = motif_dict['Protein Accessions']
    for mod_name in mod_names:
        mod_values = mods_dict['{}_value'.format(mod_name)]
        sig_mods = sig_mods_dict_modified['significant_{}_variables'.format(mod_name)]
        motif_dict[mod_name + ' Annotations'] = doug_annotations(mod_name, accessions, mod_values, sig_mods)
    return motif_dict, mod_names


def centralised_sequence(start_point, sequence, sequence_len=11):
    if sequence_len % 2 == 0:
        first_seq_section_len = second_seq_section_len = int(sequence_len / 2)
    else:
        first_seq_section_len = int((sequence_len / 2) + 1)
        second_seq_section_len = int(sequence_len / 2)
    if start_point - first_seq_section_len >= 0 and (start_point + second_seq_section_len) <= len(sequence):
        return sequence[start_point - first_seq_section_len:start_point + second_seq_section_len]
    elif start_point - first_seq_section_len < 0 and (start_point + second_seq_section_len) <= len(sequence):
        return '_' * abs(start_point - first_seq_section_len) + sequence[:start_point + second_seq_section_len]
    elif start_point - first_seq_section_len >= 0 and (start_point + second_seq_section_len) > len(sequence):
        return sequence[start_point - first_seq_section_len:] + '_' * abs(
            start_point + second_seq_section_len - len(sequence))
    else:
        return sequence


def uniprot_centralised_sequence(accessions, sig_positions, fasta_files):
    all_centralised_sequences = []
    for fasta in fasta_files:
        seqids, fasta_output = uniprot.read_fasta(fasta)
        unique_databases = set([i.split('|')[0] for i in seqids])
        centralised_sequences = []
        for i, pos_list in enumerate(sig_positions):
            i_seqs = []
            if all(pos is None for pos in pos_list):
                centralised_sequences.append([None] * len(unique_databases))
            else:
                for j, pos in enumerate(pos_list):
                    if pos is None:
                        i_seqs.append([None] * len(unique_databases))
                    else:
                        acc = accessions[i][j]
                        for database in unique_databases:
                            if fasta_checker(fasta_output, database, acc):
                                complete_seq = fasta_output['{}|{}'.format(database, acc)]['sequence']
                                p_list = []
                                for p in pos:
                                    centralised_seq = centralised_sequence(p, complete_seq)
                                    centralised_seq = ''.join(
                                        [letter.lower() if i == len(centralised_seq) // 2 else letter for i, letter in
                                         enumerate(centralised_seq)])
                                    p_list.append(centralised_seq)
                                i_seqs.append(p_list)
                            else:
                                i_seqs.append(None)
                centralised_sequences.append(i_seqs)
        all_centralised_sequences.append(centralised_sequences)
    all_centralised_sequences = gene_condensed_merge_lists(all_centralised_sequences, len(unique_databases))
    return all_centralised_sequences


def fasta_checker(fasta_output, database, accession):
    try:
        var = fasta_output["{}|{}".format(database, accession)]
        return True
    except KeyError:
        return False


def unique_sequence(seqs):
    unique_seqs = []
    for seq_list in seqs:
        if seq_list is None:
            unique_seqs.append(None)
        else:
            unique_seq_list = list(set([str(i) for i in seq_list]))
            unique_obvs = []
            for unique_seq in unique_seq_list:
                unique_seq = ast.literal_eval(unique_seq)
                if unique_seq is not None:
                    unique_obvs.append(unique_seq)
            unique_seqs.append(unique_obvs)
    return unique_seqs


def fetch_key_values(df, proteomics_accesion_id_dict, proteomics_attr_dict, accession_string='Protein Accessions'):
    attrs = []
    for acc_list in df[accession_string]:
        fc_list = []
        if acc_list is not None:
            for acc in acc_list:
                try:
                    acc_id = proteomics_accesion_id_dict[acc]
                    fc = proteomics_attr_dict[acc_id]
                    fc_list.append(fc)
                except KeyError:
                    fc_list.append(None)
        else:
            fc_list.append(None)
        attrs.append(fc_list)
    return attrs


# def fetch_key_values(df, proteomics_accesion_id_dict,proteomics_attr_dict,accession_string='Protein Accessions'):
#     attrs = []
#     vals = []
#     finalist = []
#     fc_list = []
#     for acc_list in df[accession_string]:
# #         print(acc_list)
# #         fc_list = []
#         vals.append(acc_list)
#     reqvals = [i.split("; ") for i in vals]
# #     print(vals)
#     for ele in reqvals:
#         for subele in ele:
#             finalist.append(subele)
#     for acc in finalist:
#         try:
#             acc_id = proteomics_accesion_id_dict[acc]
#             print(acc,acc_id)
#             fc = proteomics_attr_dict[acc_id]
#             fclist.append(fc)
#         except KeyError:
#                 fc_list.append(None)
#     attrs.append(fc_list)
#     return attrs
#         print(acc_list)
#         for acc in acc_list.keys():
#             print(acc)
#             try:
#                 acc_id = proteomics_accesion_id_dict[acc]
#                 fc = proteomics_attr_dict[acc_id]
#                 fc_list.append(fc)
#             except KeyError:
#                 fc_list.append(None)
#         attrs.append(fc_list)
    


def average_key_values(all_values):
    mean_values = []
    for value_list in all_values:
        if all(value is None for value in value_list):
            mean_values.append(None)
        else:
            sub_value_list = [value for value in value_list if value is not None]
            mean_values.append(np.mean(sub_value_list))
    return mean_values


def append_proteomics(phospho_df, df, id_column, accesion_id_dict, treatment_columns):
    ids = df[id_column].values
    for treatment_column in treatment_columns:
        treatments = df[treatment_column].values
        id_treatment_dict = dict([(i, value) for i, value in list(zip(ids, treatments)) if ~np.isnan(value)])
        treatment_values = fetch_key_values(phospho_df, accesion_id_dict, id_treatment_dict)
        phospho_df[treatment_column] = treatment_values
        phospho_df["Average {}".format(treatment_column)] = average_key_values(treatment_values)
    return phospho_df


# def fold_change(df, accession_string, proteomics_accesion_id_dict, proteomics_fold_change_dict):
#     proteomics_fc = []
#     for acc_list in df[accession_string]:
#         fc_list = []
#         for acc in acc_list:
#             try:
#                 acc_id = proteomics_accesion_id_dict[acc]
#                 fc = proteomics_fold_change_dict[acc_id]
#                 fc_list.append(fc)
#             except KeyError:
#                 fc_list.append(None)
#         proteomics_fc.append(fc_list)
#     return proteomics_fc

def fold_change(df, accession_string, proteomics_accesion_id_dict, proteomics_fold_change_dict):
    proteomics_fc = []
    for acc_list in df[accession_string]:
        fc_list = []
        if acc_list is not None:
            for acc in acc_list:
                try:
                    acc_id = proteomics_accesion_id_dict[acc]
                    fc = proteomics_fold_change_dict[acc_id]
                    fc_list.append(fc)
                except KeyError:
                    fc_list.append(None)
        else:
            fc_list.append(None)
        proteomics_fc.append(fc_list)
    return proteomics_fc


def average_fold_change(df, fold_change_name='Proteomics Fold Change'):
    mean_fcs = []
    for fc_list in df[fold_change_name]:
        if all(fc is None for fc in fc_list):
            mean_fcs.append(None)
        else:
            sub_fc_list = [fc for fc in fc_list if fc is not None]
            mean_fcs.append(np.mean(sub_fc_list))
    return mean_fcs


def protemoics_normalisation(motif_dict,
                             proteomics_file,
                             proteome_fields,
                             phospho_df,
                             Protemoics_Accession_string,
                             protein_accessions_list,
                             Protemoics_Contaminant_string,
                             proteome_untreated_list,
                             proteome_perturbation_list,
                             phosphoproteome_untreated_list,
                             phosphoproteome_perturbation_list):
    """
    Create a dictionary that contains each individual protein accession as a key
    and the corresponding fold change for that protein as a value.

    :param motif_dict:
    :param proteomics_file:
    :param proteome_fields:
    :param phospho_df:
    :param Protemoics_Accession_string:
    :param protein_accessions_list:
    :param Protemoics_Contaminant_string:
    :param proteome_untreated_list:
    :param proteome_perturbation_list:
    :param phosphoproteome_untreated_list:
    :param phosphoproteome_perturbation_list:
    :return:
    """

    proteomics_df = pd.read_csv(proteomics_file)

    # Create a list of column that do not appear within the df.
    missing_proteomics_columns = [column for column in proteome_fields if column not in proteomics_df.columns]

    # If the list of missing columns is empty continue else quit.
    if len(missing_proteomics_columns) > 0:
        for column in missing_proteomics_columns:
            print("Column not found within the proteome data set: " + str(column))
        quit()
    else:
        proteomics_df = proteomics_df[proteome_fields]

    proteomics_df = proteomics_df[proteomics_df[Protemoics_Contaminant_string] == 0]
    untreated = proteomics_df[proteome_untreated_list]
    untreated.index = range(len(untreated))
    perturbation = proteomics_df[proteome_perturbation_list]
    perturbation.index = range(len(perturbation))

    proteomics_fold_change = [np.mean(perturbation.values[i]) / np.mean(untreated.values[i]) for i in
                              range(len(proteomics_df))]
    # Normalize to total protein (from Proteomics dataset)
    proteomics_fold_change_dict = dict(zip(proteomics_df[Protemoics_Accession_string].tolist(), proteomics_fold_change))

    Abundances_Scaled = phosphoproteome_untreated_list + phosphoproteome_perturbation_list

    for column in Abundances_Scaled:
        motif_dict[column] = []
        Abundances_Scaled_list = phospho_df[column].tolist()

        for i in range(len(protein_accessions_list)):
            for j in range(len(protein_accessions_list[i])):
                motif_dict[column].append(Abundances_Scaled_list[i])

    return proteomics_fold_change_dict, proteomics_df


def filter_mod_column(df, mod_list):
    for mod in mod_list:
        retain_annotation = []
        doug_annotations = df['{} Annotations'.format(mod)]
        for annotation in doug_annotations:
            if annotation is None:
                retain_annotation.append(False)
            else:
                if any(mod in str(annotation) for mod in mod_list):
                    retain_annotation.append(True)
                else:
                    retain_annotation.append(False)
        df = df[retain_annotation]
        # Write this motif_df df to the output file location as CSV.
        df = df.dropna(subset=['{} Annotations'.format(mod)])
    return df


def display_results(significant_proteins, total_proteins, fdr_percentage):
    """
    Print to the cmd line the results of the analysis

    Args:
        significant_proteins: number of significant proteins.
        total_proteins: total number of proteins
        fdr_percentage: fdr percentage used for the analysis (i.e. 0.05)

    Returns:
        prints meaningful results to the cmd line.

    Raises:
        N/A
    """

    print('Number of significant proteins: ' + str(significant_proteins))
    print(str(significant_proteins / total_proteins) + '% of the identified at the set FDR')
    print(str(fdr_percentage * significant_proteins) + ' total false positive hits')


def volcano_plot(df, title, output_png, fdr_value):
    sns.set_style('white', {'legend.frameon': True})
    ax = sns.lmplot('log2(Fold Change)', '-log10(P-value)',
                    data=df,
                    fit_reg=False,
                    scatter_kws={"s": 10},
                    hue='FDR',
                    size=8,
                    aspect=1.5, legend_out=False)

    # Title both the plot and the axes.
    plt.title('Volcano Plot of ' + title)
    plt.xlabel(r'$\log_{2}(Fold \, Change)$')
    plt.ylabel(r'$-\log_{10}(P-Value)$')

    # title

    leg = ax.axes.flat[0].get_legend()
    new_title = 'False Discovery Rate ({}%)'.format(fdr_value * 100)
    leg.set_title(new_title)
    sig_count = Counter(df['FDR'])
    sig_percent = str(round((sig_count[1] / df.shape[0]) * 100, 2))
    insig_percent = str(round((sig_count[0] / df.shape[0]) * 100, 2))
    new_labels = ['{}% Insignificant'.format(insig_percent), '{}% Significant'.format(sig_percent)]
    for t, l in zip(leg.texts, new_labels): t.set_text(l)

    annotation = "{} Significant Phosphopeptides".format(sig_count[1])

    ax.ax.text(2.15, 5.45, annotation, size='small', color='black')

    # Save the plot to the destination specified.
    ax.savefig(output_png)


def refine_columns(columns):
    """
    :param columns:
    :return:
    """
    unnecessary_word = []
    for i in range(0, len(columns) - 1):
        for j in range(1, len(columns)):
            unnecessary_word.append(list(set(columns[i].split(' ')).intersection(columns[j].split(' '))))
    unnecessary_word = [i for sublist in unnecessary_word for i in sublist]
    unnecessery_count = Counter(unnecessary_word)
    max_value = max(unnecessery_count.values())
    unnecessary_word = [key for key, value in unnecessery_count.items() if value == max_value]
    new_columns = []
    for col in columns:
        new_columns.append(' '.join([i for i in col.split(' ') if i not in unnecessary_word]))
    return new_columns


def split_experiment(columns, experiment_types):
    """
    :param columns:
    :param experiment_types:
    :return:
    """
    experiments = {}
    refined_columns = refine_columns(columns)
    for exp_type in experiment_types:
        experiments[exp_type] = [i for i in refined_columns if exp_type in i]
    return experiments


def pca_analysis(df, columns, n_components):
    """
    :param df:
    :param columns:
    :param n_components:
    :return:
    """
    try:
        pca = PCA(n_components=n_components)
        pca.fit_transform(df[columns].dropna())
        index_names = ['PC-{}'.format(i) for i in range(1, n_components + 1)]
        pca_df = pd.DataFrame(pca.components_, columns=df[columns].dropna().columns, index=index_names).T
        pca_df['Labels'] = refine_columns(pca_df.index)
        print(list(zip(pca_df.columns, pca.explained_variance_ratio_)))
        return pca, pca_df
    except ValueError:
        print("The number of components must be less than the number of columns used within the PCA analysis.")
        return None, None


def label_point(x, y, val, ax):
    """
    :param x:
    :param y:
    :param val:
    :param ax:
    :return:
    """
    a = pd.concat({'x': x, 'y': y, 'val': val}, axis=1)
    for i, point in a.iterrows():
        ax.text(point['x'] + .02, point['y'], str(point['val']))


def pca_plot(pca_output, pca_output_filepath, variance_ratio):
    fig, ax = plt.subplots(figsize=(16, 9))
    ax = sns.regplot(x=pca_output.columns[0], y=pca_output.columns[1], data=pca_output, ci=None, fit_reg=False)
    plt.title('PCA Plot')
    # Set x-axis label
    plt.xlabel('PC-1: {}%'.format(str(round(variance_ratio[0] * 100, 2))))
    # Set y-axis label
    plt.ylabel('PC-2: {}%'.format(str(round(variance_ratio[1] * 100, 2))))
    label_point(pca_output['PC-1'], pca_output['PC-2'], pca_output['Labels'], plt.gca())
    plt.savefig(pca_output_filepath)
    plt.show()


def anova(df, columns, experiment_types):
    """
    :param df:
    :param columns:
    :param experiment_types:
    :return:
    """
    experiment_values = df[columns].values

    exp_index = {}
    for exp_type in experiment_types:
        exp_index[exp_type] = [i for i, value in enumerate(columns) if exp_type in value]

    anova_output = []
    for i in range(df.shape[0]):
        anova_output.append(f_oneway(*[experiment_values[i, exp_index[exp_type]] for exp_type in experiment_types]))

    anova_p_value = [i[1] for i in anova_output]
    anova_f_value = [i[0] for i in anova_output]

    return anova_f_value, anova_p_value


def pathway_genes_checker(df, gene_column, applicable_genes):
    """
    :param df:
    :param gene_column:
    :param applicable_genes:
    :return:
    """
    check_results = []
    for gene_list in df[gene_column]:
        if gene_list is not None:
            if any(gene in applicable_genes for gene in gene_list):
                check_results.append(True)
            else:
                check_results.append(False)
        else:
            check_results.append(False)
    return check_results


def multi_comparison(df, fdr_column, p_value_column, proteomics_columns, experiment_types):
    """
    :param df:
    :param fdr_column:
    :param p_value_column:
    :param proteomics_columns:
    :param experiment_types:
    :return:
    """
    mc_reject = []
    condition = df[fdr_column] > df[p_value_column]
    df_subset = df.loc[:condition[condition == True].index[-1], :]

    exp_index = {}
    for exp_type in experiment_types:
        exp_index[exp_type] = [i for i, value in enumerate(proteomics_columns) if exp_type in value]

    test_labels = [i for sublist in [([key] * len(value), value)[0] for key, value in exp_index.items()] for i in
                   sublist]

    for i in range(df_subset.shape[0]):
        input_data = np.rec.array(list(zip(test_labels, df_subset[proteomics_columns].iloc[i].values)),
                                  dtype=[('Treatment', '|U5'), ('Value', '<i8')])
        mc = MultiComparison(input_data['Value'], input_data['Treatment'])
        mc_reject.append(mc.tukeyhsd().reject)

    mc_reject = np.array(mc_reject)

    group1 = []
    group2 = []
    result = mc.tukeyhsd()
    for i in range(1, len(result.summary())):
        group1.append(str(result.summary()[i][0]))
        group2.append(str(result.summary()[i][1]))

    mc_columns = ["MultiComparison {}_{}".format(group1[i], group2[i]) for i in range(len(group1))]

    for i, col in enumerate(mc_columns):
        df_subset[col] = mc_reject[:, i]

    return df_subset


def extract_motif(df, seq_list, motif_range):
    df_subset = df[df.iloc[:, 0].isnull() == False]
    Centralised_Sequence_values = df_subset[[i for i in df_subset.columns if 'Centralised Sequence' in i]].values
    retain_seq = []
    for i in range(len(Centralised_Sequence_values)):
        seq_checker = []
        for seq in Centralised_Sequence_values[i]:
            if seq is not None:
                if any(i in str(seq)[motif_range[0]:motif_range[1]] for i in seq_list):
                    seq_checker.append(True)
                else:
                    seq_checker.append(False)
            else:
                seq_checker.append(False)
        if sum(seq_checker) > 0:
            retain_seq.append(True)
        else:
            retain_seq.append(False)

    return df_subset[retain_seq]

# def run_analysis(phosphoproteomics_df,
#                  phosphoproteome_dataset,
#                  fdr_value,
#                  doug_mod_filter_list,
#                  filter_doug_annotations,
#                  motif_analysis,
#                  motif_list,
#                  motif_range,
#                  order_by_column):
#     """
#
#     :param phosphoproteomics_df:
#     :param phosphoproteome_dataset:
#     :param fdr_value:
#     :param doug_mod_filter_list:
#     :param filter_doug_annotations:
#     :param motif_analysis:
#     :param motif_list:
#     :param motif_range:
#     :param order_by_column:
#     :return:
#     """
#
#     if filter_doug_annotations is False:
#         # Allocates a name to the volcano plot.
#         volcano_plot_png = phosphoproteome_dataset.replace('.csv', '') + " volcano_plot" + " (FDR " + str(
#             fdr_value) + ").png"
#     else:
#         # Allocates a name to the volcano plot.
#         volcano_plot_png = phosphoproteome_dataset.replace('.csv', '') + " volcano_plot " + str(
#             doug_mod_filter_list) + " (FDR " + str(
#             fdr_value) + ").png"
#
#     # Run the volcano_plot function which creates the plot.
#     volcano_plot(phosphoproteomics_df, phosphoproteome_dataset, filter_doug_annotations, doug_mod_filter_list,
#                  "Phosphoproteome_Output/" + volcano_plot_png, 6, 2)
#
#     if motif_analysis is True:
#
#         if filter_doug_annotations is False:
#             motif_output_file = 'Phosphoproteome_Output/' + phosphoproteome_dataset.replace('.csv',
#                                                                                             '') + ' ' + str(
#                 motif_list) + ' (FDR ' + str(
#                 fdr_value) + ').csv'  # In .csv format!
#         else:
#             motif_output_file = 'Phosphoproteome_Output/' + phosphoproteome_dataset.replace('.csv',
#                                                                                             '') + ' ' + str(
#                 motif_list) + str(
#                 doug_mod_filter_list) + ' (FDR ' + str(
#                 fdr_value) + ').csv'  # In .csv format!
#
#         phosphoproteomics_motif_df = extract_motif(phosphoproteomics_df, motif_list, motif_range)
#
#         if filter_doug_annotations is True:
#             # Write this motif_df df to the output file location as CSV.
#             # phosphoproteomics_motif_df = phosphoproteomics_motif_df.sort_index(axis=0)
#             phosphoproteomics_motif_df = phosphoproteomics_motif_df.sort_values(by=[order_by_column],
#                                                                                 ascending=False)
#             phosphoproteomics_motif_df = phosphoproteomics_motif_df.dropna(subset=['Doug Annotations'])
#             phosphoproteomics_motif_df.to_csv(motif_output_file, index=False)
#         else:
#             # Write this motif_df df to the output file location as CSV.
#             # phosphoproteomics_motif_df.sort_index(axis=0).to_csv(motif_output_file, index=False)
#             phosphoproteomics_motif_df.sort_values(by=[order_by_column], ascending=False).to_csv(
#                 motif_output_file, index=False)
#
#         if filter_doug_annotations is False:
#             # Allocates a name to the volcano plot.
#             motif_volcano_plot_png = phosphoproteome_dataset.replace('.csv', '') + " volcano_plot " + str(
#                 motif_list) + " (FDR " + str(
#                 fdr_value) + ").png"
#         else:
#             # Allocates a name to the volcano plot.
#             motif_volcano_plot_png = phosphoproteome_dataset.replace('.csv', '') + " volcano_plot " + str(
#                 motif_list) + str(
#                 doug_mod_filter_list) + " (FDR " + str(
#                 fdr_value) + ").png"
#
#         # Run the volcano_plot function which creates the plot.
#         volcano_plot(phosphoproteomics_motif_df, phosphoproteome_dataset, filter_doug_annotations, doug_mod_filter_list,
#                      "Phosphoproteome_Output/" + motif_volcano_plot_png, 6, 2)

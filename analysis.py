import re
import sys

from scipy.stats import ttest_ind

from uclacode.utils import *


class Phosphoproteomics(object):
    def __init__(self, file_path, phospho_file, phospho_fields, contaminant_string='Contaminant'):
        self.phospho_file = phospho_file
        self.phospho_fields = phospho_fields

        # Lets create a dictionary that contains the observations of each individual protein accession.
        self.dict = dict()
        self.dict['Protein Accessions'] = list()
        self.dict['Modifications (all possible sites)'] = list()
        self.dict['Protein Accession Ranges'] = list()
        self.dict['Sequence'] = list()

        self.mods_dict = dict()
        self.sig_mods_dict = dict()
        self.lhs_range = list()
        self.sig_mods_dict_modified = dict()

        create_dir(file_path)

        # Begin preparing the data set.
        self.df = pd.read_csv(self.phospho_file)
        self.origional_df = self.df.copy()

        # Create a list of column that do not appear within the df.
        missing_phospho_columns = [column for column in self.phospho_fields if column not in self.df.columns]

        # If the list of missing columns is empty continue else quit.
        if len(missing_phospho_columns) > 0:
            for column in missing_phospho_columns:
                print("Column not found within the phosphoproteomics data set: " + str(column))
            sys.exit()
        else:
            self.df = self.df[self.phospho_fields]

        if np.sum(self.df[contaminant_string].apply(self.int_tester).isnull()) > 0:
            print("The contaminant column must be integer based {Yes: 1, No: 0}.")
        else:
            self.df = self.df[self.df[contaminant_string] == 0]

    def int_tester(self, x):
        try:
            return int(x)
        except ValueError:
            return None

    def float_tester(self, x):
        try:
            return float(x)
        except ValueError:
            return None

    def filter_column(self, column, value):
        if column in self.df.columns:
            print("Removing {} values...".format(value))
            old_shape = self.df.shape
            self.df = self.df[self.df[column].astype(str) != value]
            print("Removed {} {} values".format((old_shape[0] - self.df.shape[0]), value))
        else:
            print("{} not found in dataset".format(column))
        return self

    def filter_contaminant(self, df, column, value):
        if column in df.columns:
            df = df[df[column] == value]
        else:
            print("{} not found in dataset".format(column))
        return df

    def filter_null(self, column):
        if column in self.df.columns:
            print("Removing {} values...".format('Null'))
            old_shape = self.df.shape
            self.df = self.df[~self.df[column].isnull()]
            print("Removed {} {} values".format((old_shape[0] - self.df.shape[0]), 'Null'))
        else:
            print("{} not found in Phosphoproteomics dataset".format(column))
        return self.df

    def process_data(self, contaminant_string, sequence_string, accession_string, modification_string, fasta_files):

        self.df = self.filter_contaminant(self.df, contaminant_string, False)

        accessions = [i.split('; ') for i in self.df[accession_string]]
        sequences = self.df[sequence_string].tolist()
        modifications = self.df[modification_string].tolist()
        self.dict['Protein Accessions'] = accessions
        self.dict['Modifications (all possible sites)'] = self.df[modification_string].tolist()
        self.dict['Sequence'] = sequences

        complete_ranges = findall_ranges(accessions, sequences, fasta_files)
        self.dict['Protein Accession Ranges'] = complete_ranges

        # Create a list of lists that c ontains the individual modifications that are carried out on each observation.
        motif_mods = [i.split(']; ') for i in self.dict[modification_string]]
        motif_mods = [[i + ']' if i.find(']') == -1 else i for i in motif_mods[j]] for j in range(len(motif_mods))]

        # Determine the key features that appear in the modifications column.
        self.unique_mod_types = findall_unique_mod_types(modifications)

        # Create a dictionary that breaks the modifcations column and creates individual
        # lists for each key feature of the modifications.
        self.mods_dict = mods_dict_updater(self.mods_dict, self.unique_mod_types, motif_mods)

        # Create lists for the LHS and RHS of the range for each entry of the sequence list
        # using the UniProt fasta file.
        self.lhs_range = findall_uniprot_ranges(self.dict)

        all_protein_names, all_entry_names, all_gene_names = findall_uniprot_protein_names(accessions, fasta_files)

        self.dict['Protein Name'] = all_protein_names
        self.dict['Protein Entry Name'] = all_entry_names
        self.dict['Gene Name'] = all_gene_names

        # Create a dictionary that breaks the modifications column and creates individual lists for
        # each key feature of the modifications that is statistically significant.
        self.sig_mods_dict = sig_mods_dict_updater(self.mods_dict, self.sig_mods_dict, self.unique_mod_types)

        # Create a dictionary that contains individual lists for each key feature
        # of the modifications that is statistically significant.
        # The lists within this dictionary will then eventually be brought together
        # to create the Doug Annotations column.
        self.sig_mods_dict_modified = sig_mods_dict_modified_updater(self.sig_mods_dict, self.sig_mods_dict_modified,
                                                                     self.lhs_range)

        sig_var_keys = [key for key in self.sig_mods_dict_modified.keys() if '_variables_' not in key]

        if len(sig_var_keys) > 0:
            for key in sig_var_keys:
                match = re.search('significant_(.*)_variables', key)
                column = "{} Site".format(match.group(1))

                col_sig_pos = []
                for positions in self.sig_mods_dict_modified['significant_Phospho_variables']:
                    if positions[0] is None:
                        col_sig_pos.append(None)
                    else:
                        col_sig_p = []
                        for p_list in positions:
                            if p_list is not None:
                                col_sub_sig_p = []
                                for p in p_list:
                                    col_sub_sig_p.append(re.sub("[\(\[].*?[\)\]]", "", p))
                                col_sig_p.append(col_sub_sig_p)
                            col_sig_pos.append(col_sig_p)

                self.dict[column] = col_sig_pos

        # # Determine Doug Annotations and append then to the dict.
        self.dict, self.mod_names = add_doug_annotations(self.dict, self.mods_dict,
                                                         self.sig_mods_dict_modified)

        # Determine centralised sequences and append then to the dict.
        for name in self.mod_names:
            significant_positions = self.sig_mods_dict_modified['significant_{}_variables_positions'.format(name)]
            self.dict["{} Centralised Sequence".format(name)] = uniprot_centralised_sequence(accessions,
                                                                                             significant_positions,
                                                                                             fasta_files)
        for mod in self.mod_names:
            self.mod_stats(mod)

        return self

    def mod_stats(self, mod):
        class_1_percentage_found = len([i for i in self.dict['{} Annotations'.format(mod)] if i != [None]]) / len(
            self.dict['{} Annotations'.format(mod)])
        mod_count = len([i for i in self.dict['{} Annotations'.format(mod)] if i != [None]])
        mod_sum = sum([int(i) for i in self.mods_dict['{}_value'.format(mod)] if i is not None])

        print("Class one {} peptide percentage found: {}".format(mod, class_1_percentage_found))
        print("Class one {} peptide count: {}".format(mod, mod_count))
        print("{} sum: {}".format(mod, mod_sum))

        phospho_variables = [i.replace('[', '').replace(']', '').split('; ') for i in
                             self.mods_dict['Phospho_variables'] if i != None]

        phospho_percentage = []
        for phospho_vars in phospho_variables:
            for phospho_var in phospho_vars:
                phospho_percentage.append(phospho_var.partition('(')[-1][:-1])

        phospho_perc_df = pd.DataFrame(Counter(phospho_percentage).most_common())
        phospho_perc_df[0] = phospho_perc_df[0].apply(self.float_tester)
        phospho_perc_df = phospho_perc_df.sort_values(0, ascending=False)
        phospho_perc_df.columns = ['Percentage', 'Count']
        csv_file = '{}_percentage_count.csv'.format(mod)
        phospho_perc_df.to_csv(csv_file, index=False)
        print("{} count saved to: {}".format(mod, csv_file))

    def read_proteomics(self, file_path, fields, contaminant_string='Contaminant'):

        self.proteomics_df = pd.read_csv(file_path)
        self.origional_proteomics_df = self.proteomics_df.copy()

        # Create a list of column that do not appear within the df.
        missing_proteomics_columns = [column for column in fields if column not in self.proteomics_df.columns]

        # If the list of missing columns is empty continue else quit.
        if len(missing_proteomics_columns) > 0:
            for column in missing_proteomics_columns:
                print("Column not found within the proteomics data set: " + str(column))
            quit()
        else:
            self.proteomics_df = self.proteomics_df[fields]

        self.proteomics_df = self.filter_contaminant(self.proteomics_df, contaminant_string, False)

        return self

    # This function will return a dict that has unique protein found within the proteomics data set as keys and
    # the corresponding fold change values for that data set as the values.
    def proteomics_fold_change(self, accession_string, id_string, untreated_list, perturbation_list):
        self.proteomics_accession_id = id_string
        self.proteomics_accession_string = accession_string
        self.proteomics_untreated_list = untreated_list
        self.proteomics_perturbation_list = perturbation_list

        untreated = self.proteomics_df[untreated_list]
        untreated.index = range(len(untreated))
        perturbation = self.proteomics_df[perturbation_list]
        perturbation.index = range(len(perturbation))

        proteomics_fold_change = [np.mean(perturbation.values[i]) / np.mean(untreated.values[i]) for i in
                                  range(len(self.proteomics_df))]
        # Normalize to total protein (from Proteomics dataset)
        id_fold_change = list(zip(self.proteomics_df[self.proteomics_accession_id].tolist(), proteomics_fold_change))
        id_fold_change = [(i, value) for i, value in id_fold_change if ~np.isnan(value)]
        self.proteomics_fold_change_dict = dict(id_fold_change)
        self.proteomics_accesion_id_dict = dict(self.proteomics_df[[accession_string, id_string]].values)
        return self
    
    def normalize_data(self, phospho_untreated_list, phospho_perturbation_list):

        self.phospho_untreated_list = phospho_untreated_list
        self.phospho_perturbation_list = phospho_perturbation_list
        abundances_scaled = phospho_untreated_list + phospho_perturbation_list

        for column in abundances_scaled:
            self.dict[column] = []
            abundances_scaled_list = self.df[column].tolist()
            for i in range(self.df.shape[0]):
                self.dict[column].append(abundances_scaled_list[i])

        # for key, values in self.dict.items():
        #     print(key, len(values))

        self.df = pd.DataFrame.from_dict(self.dict,orient='index')
        self.df = self.df.transpose()

        self.df['Untreated Mean'] = self.df[phospho_untreated_list].mean(axis=1)
        self.df['Perturbation Mean'] = self.df[phospho_perturbation_list].mean(axis=1)
        self.df['Phosphoproteomics Fold Change'] = self.df['Perturbation Mean'] / self.df[
            'Untreated Mean']

        self.proteomics_df['Untreated Mean'] = self.proteomics_df[self.proteomics_untreated_list].mean(axis=1)
        self.proteomics_df['Perturbation Mean'] = self.proteomics_df[self.proteomics_perturbation_list].mean(axis=1)
        self.proteomics_df['Proteomics Fold Change'] = self.proteomics_df['Perturbation Mean'] / self.proteomics_df[
            'Untreated Mean']

        for col in abundances_scaled:
            self.proteomics_df['Proteomics ' + col] = self.proteomics_df[col]

        self.df['Proteomics Fold Change'] = fold_change(self.df, 'Protein Accessions',
                                                        self.proteomics_accesion_id_dict,
                                                        self.proteomics_fold_change_dict)

        self.df['Average Proteomics Fold Change'] = average_fold_change(self.df)

        self.df['Normalized Fold Change'] = self.df['Phosphoproteomics Fold Change'] / self.df[
            'Average Proteomics Fold Change']

        proteomics_abundances_scaled = ['Proteomics ' + col for col in abundances_scaled]
        self.df = append_proteomics(self.df, self.proteomics_df, self.proteomics_accession_id,
                                    self.proteomics_accesion_id_dict, proteomics_abundances_scaled)

        for col in abundances_scaled:
            self.df['Protein Normalized {}'.format(col)] = (100 / self.df[
                'Average Proteomics {}'.format(col)]) * \
                                                           self.df[col]

        norm_abundances_scaled = ['Protein Normalized {}'.format(col) for col in abundances_scaled]

        self.df['Protein Normalized'] = self.df[norm_abundances_scaled].notnull().all(axis=1)

        return self

    def filter_mods(self, mod_list):
        self.df = filter_mod_column(self.df, mod_list)
        return self

    def to_csv(self, output_file):
        self.df.to_csv(output_file, index=False)

    def t_test_analysis(self, treatment_list=None, phospho_untreated_list=None, phospho_perturbation_list=None,
                        fdr_percentage=0.01):
        """

        :param treatment_list: A list of key words that define each experiment being carried out
                               i.e.  ['KRASi', 'FGFRi', 'Combo'].
        :param phospho_untreated_list: A list of column names that contain the untreated results.
        :param phospho_perturbation_list: A list of column names that contain all* the treated results.
                               All the columns that contain the key words found in the treatment_list variable.
        :param fdr_percentage: A float i.e. 0.05.
        :return: A DataFrame containing the statistical results from all of the T-Tests carried out.
        """

        # Check that a correct value has been assigned to the fdr percentage.
        if type(fdr_percentage) is not float or fdr_percentage <= 0 or fdr_percentage >= 1:
            print('Incorrect FDR percentage chosen.')
            quit()

        if treatment_list is None:
            if phospho_untreated_list is None:
                phospho_untreated_list = self.phospho_untreated_list
            if phospho_perturbation_list is None:
                phospho_perturbation_list = self.phospho_perturbation_list

            norm_untreated_abundances_scaled = ['Protein Normalized {}'.format(col) for col in
                                                phospho_untreated_list]
            norm_perturbation_abundances_scaled = ['Protein Normalized {}'.format(col) for col in
                                                   phospho_perturbation_list]

            normalized_t_statistic = []
            normalized_p_value = []
            normalized_fold_change = []
            normalized_log_2_fold_change = []
            normalized_untreated_list = self.df[norm_untreated_abundances_scaled].values
            normalized_perturbation_list = self.df[norm_perturbation_abundances_scaled].values
            for i in range(self.df.shape[0]):
                normalized_t_statistic.append(
                    ttest_ind(normalized_untreated_list[i], normalized_perturbation_list[i])[0])
                normalized_p_value.append(ttest_ind(normalized_untreated_list[i], normalized_perturbation_list[i])[1])
                normalized_fold_change.append(
                    np.mean(normalized_perturbation_list[i]) / np.mean(normalized_untreated_list[i]))
                normalized_log_2_fold_change.append(np.log2(normalized_fold_change[i]))

            self.df['P-value'] = normalized_p_value
            self.df['T-Statistic'] = normalized_t_statistic
            self.df['Fold Change'] = normalized_fold_change
            self.df['log2(Fold Change)'] = normalized_log_2_fold_change

            self.df = self.df.sort_values(by='P-value', ascending=True)
            self.df['Rank'] = range(1, len(self.df['log2(Fold Change)']) + 1)
            total_proteins = sum(self.df['Protein Normalized'])
            fdr_percentage_value = fdr_percentage
            self.df['FDR Values'] = (self.df['Rank'] / total_proteins) * fdr_percentage_value
            self.df['FDR Calculation'] = fdr_percentage * (self.df['Rank'] / total_proteins)
            last_true_value = (self.df['P-value'] < self.df['FDR Calculation'])[::-1].idxmax()
            if int(last_true_value) != int(self.df.index[-1]):
                num_sigs = self.df.index.get_loc(last_true_value) + 1
            else:
                num_sigs = 0
            num_non_sigs = self.df.shape[0] - num_sigs
            self.df['FDR'] = [1] * num_sigs + [0] * num_non_sigs
            significant_proteins = self.df['FDR'].sum()
            self.df['-log10(P-value)'] = -np.log10(self.df['P-value'])

            # Displays the number of significant protein within the data set.
            display_results(significant_proteins, total_proteins, fdr_percentage)

        else:

            for treatment in treatment_list:

                if phospho_untreated_list is None:
                    untreated_list = self.phospho_untreated_list
                else:
                    untreated_list = [col for col in phospho_untreated_list if 'NT' in col]
                if phospho_perturbation_list is None:
                    perturbation_list = [col for col in self.phospho_perturbation_list if treatment in col]
                else:
                    perturbation_list = [col for col in phospho_perturbation_list if treatment in col]

                normalized_t_statistic = []
                normalized_p_value = []
                normalized_fold_change = []
                normalized_log_2_fold_change = []
                normalized_untreated_list = self.df[untreated_list].values
                normalized_perturbation_list = self.df[perturbation_list].values
                for i in range(self.df.shape[0]):
                    normalized_t_statistic.append(
                        ttest_ind(normalized_untreated_list[i], normalized_perturbation_list[i])[0])
                    normalized_p_value.append(
                        ttest_ind(normalized_untreated_list[i], normalized_perturbation_list[i])[1])
                    normalized_fold_change.append(
                        np.mean(normalized_perturbation_list[i]) / np.mean(normalized_untreated_list[i]))
                    normalized_log_2_fold_change.append(np.log2(normalized_fold_change[i]))

                self.df['{} P-value'.format(treatment)] = normalized_p_value
                self.df['{} T-Statistic'.format(treatment)] = normalized_t_statistic
                self.df['{} Fold Change'.format(treatment)] = normalized_fold_change
                self.df['{} log2(Fold Change)'.format(treatment)] = normalized_log_2_fold_change

                self.df = self.df.sort_values(by='{} P-value'.format(treatment), ascending=True)
                self.df['{} Rank'.format(treatment)] = range(1,
                                                             len(self.df['{} log2(Fold Change)'.format(treatment)]) + 1)
                total_proteins = sum(self.df['Protein Normalized'])
                fdr_percentage_value = fdr_percentage
                self.df['{} FDR Values'.format(treatment)] = (self.df['{} Rank'.format(
                    treatment)] / total_proteins) * fdr_percentage_value

                self.df['{} FDR Calculation'.format(treatment)] = fdr_percentage * (
                        self.df['{} Rank'.format(treatment)] / total_proteins)

                last_true_value = (self.df['{} P-value'.format(treatment)] < self.df[
                    '{} FDR Calculation'.format(treatment)])[::-1].idxmax()
                if int(last_true_value) != int(self.df.index[-1]):
                    num_sigs = self.df.index.get_loc(last_true_value) + 1
                else:
                    num_sigs = 0
                num_non_sigs = self.df.shape[0] - num_sigs
                self.df['{} FDR'.format(treatment)] = [1] * num_sigs + [0] * num_non_sigs
                significant_proteins = self.df['{} FDR'.format(treatment)].sum()
                self.df['{} -log10(P-value)'.format(treatment)] = -np.log10(self.df['{} P-value'.format(treatment)])

                print("Treatment: {}".format(treatment))
                # Displays the number of significant protein within the data set.
                display_results(significant_proteins, total_proteins, fdr_percentage)
                print("\n")

        return self

    def anova_analysis(self, untreated_list, perturbation_list, fdr_percentage, experiment_types):

        # Check that a correct value has been assigned to the fdr percentage.
        if type(fdr_percentage) is not float or fdr_percentage <= 0 or fdr_percentage >= 1:
            print('Incorrect FDR percentage chosen.')
            quit()

        # Break the experiment columns into individual types.
        columns = untreated_list + perturbation_list
        self.df = self.df[~self.df[columns].isnull().any(axis=1)]

        # Carry out a One-Way ANOVA.
        anova_f_value, anova_p_value = anova(self.df, columns, experiment_types)
        self.df['ANOVA P-value'] = anova_p_value
        self.df['ANOVA F-Statistic'] = anova_f_value
        self.df = self.df.sort_values(['ANOVA P-value'], ascending=True)

        self.df['Rank'] = np.arange(1, self.df.shape[0] + 1)

        total_proteins = len(self.df['ANOVA P-value'].dropna())

        # Determine the significant proteins by carrying out an FDR calculation.
        self.df['FDR Calculation'] = fdr_percentage * (self.df['Rank'] / total_proteins)
        fdr = list(self.df['FDR Calculation'] > self.df['ANOVA P-value'])
        self.df['FDR'] = [1 if fdr[i] is True else 0 for i in range(len(fdr))]
        significant_proteins = self.df['FDR'].sum()

        # Displays the number of significant protein within the data set.
        display_results(significant_proteins, total_proteins, fdr_percentage)

        return self
